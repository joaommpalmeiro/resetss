# resetss

Different CSS resets ready to be used.

- [Source code](https://gitlab.com/joaommpalmeiro/resetss)
- [npm package](https://www.npmjs.com/package/resetss)
- [Licenses](https://licenses.dev/npm/resetss/0.3.0)

## CSS Resets

### Table of Contents

1. [Josh W. Comeau's CSS Reset](#josh-w-comeaus-css-reset)
2. [A Modern CSS Reset by Andy Bell](#a-modern-css-reset-by-andy-bell)
3. [Preflight by Tailwind CSS](#preflight-by-tailwind-css)
4. [modern-normalize by Sindre Sorhus](#modern-normalize-by-sindre-sorhus)
5. [destyle.css by Nicolas Cusan](#destylecss-by-nicolas-cusan)

### Josh W. Comeau's CSS Reset

| File                                                 | Source                                                         | Version        |
| ---------------------------------------------------- | -------------------------------------------------------------- | -------------- |
| [`josh-comeau-reset.css`](src/josh-comeau-reset.css) | [Blog post](https://www.joshwcomeau.com/css/custom-css-reset/) | June 9th, 2023 |

#### Import

```js
import "resetss/josh-comeau-reset.css";
```

#### Nuxt (`nuxt.config.ts`)

```ts
export default defineNuxtConfig({
  css: ["resetss/josh-comeau-reset.css"],
});
```

### A Modern CSS Reset by Andy Bell

| File                                               | Source                                                                                                                                                                                                    | Version                                                               |
| -------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------- |
| [`modern-css-reset.css`](src/modern-css-reset.css) | [Blog post](https://piccalil.li/blog/a-more-modern-css-reset/) and [Repo](https://github.com/Set-Creative-Studio/cube-boilerplate/blob/f4bdbb0293ea685084818371026ec8c16577e416/src/css/global/reset.css) | 18th of September 2023 and `f4bdbb0293ea685084818371026ec8c16577e416` |

#### Import

```js
import "resetss/modern-css-reset.css";
```

#### Nuxt (`nuxt.config.ts`)

```ts
export default defineNuxtConfig({
  css: ["resetss/modern-css-reset.css"],
});
```

### Preflight by Tailwind CSS

| File                                                   | Source                                                                                | Version  |
| ------------------------------------------------------ | ------------------------------------------------------------------------------------- | -------- |
| [`preflight-tailwind.css`](src/preflight-tailwind.css) | [Repo](https://github.com/tailwindlabs/tailwindcss/blob/v3.4.1/src/css/preflight.css) | `v3.4.1` |

#### Import

```js
import "resetss/preflight-tailwind.css";
```

#### Nuxt (`nuxt.config.ts`)

```ts
export default defineNuxtConfig({
  css: ["resetss/preflight-tailwind.css"],
});
```

### modern-normalize by Sindre Sorhus

| File                                               | Source                                                                                    | Version  |
| -------------------------------------------------- | ----------------------------------------------------------------------------------------- | -------- |
| [`modern-normalize.css`](src/modern-normalize.css) | [Repo](https://github.com/sindresorhus/modern-normalize/blob/v2.0.0/modern-normalize.css) | `v2.0.0` |

#### Import

```js
import "resetss/modern-normalize.css";
```

#### Nuxt (`nuxt.config.ts`)

```ts
export default defineNuxtConfig({
  css: ["resetss/modern-normalize.css"],
});
```

### destyle.css by Nicolas Cusan

| File                             | Source                                                                       | Version  |
| -------------------------------- | ---------------------------------------------------------------------------- | -------- |
| [`destyle.css`](src/destyle.css) | [Repo](https://github.com/nicolas-cusan/destyle.css/blob/v4.0.1/destyle.css) | `v4.0.1` |

#### Import

```js
import "resetss/destyle.css";
```

#### Nuxt (`nuxt.config.ts`)

```ts
export default defineNuxtConfig({
  css: ["resetss/destyle.css"],
});
```

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version && npm --version
```

```bash
npm install
```

```bash
npm run format
```

### Preflight by Tailwind CSS

For declarations whose property value is set via the `theme()` function, manually replace it with the default value (second argument). Then, run the following command to check if the output is as expected after the previous changes and discard other possible changes:

```bash
npx tailwindcss --input src/preflight-tailwind.css --output src/preflight-tailwind.css
```

## Deployment

```bash
npm pack --dry-run
```

```bash
npm version patch
```

```bash
npm version minor
```

```bash
npm version major
```

- Update the version in the `Licenses` link at the top.
- Commit and push changes.
- Create a tag on [GitHub Desktop](https://github.blog/2020-05-12-create-and-push-tags-in-the-latest-github-desktop-2-5-release/).
- Check [GitLab](https://gitlab.com/joaommpalmeiro/resetss/-/tags).

```bash
npm login
```

```bash
npm publish
```

- Check [npm](https://www.npmjs.com/package/resetss).
