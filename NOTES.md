# Notes

- https://github.com/joaopalmeiro/social-media-icons
- https://byby.dev/css-resets
- https://en.wikipedia.org/wiki/Reset_style_sheet
- https://github.com/Set-Creative-Studio/cube-boilerplate/tree/main/src/css/global
- https://github.com/biomejs/biome/issues/1285
- https://prettier.io/docs/en/install
- https://github.com/evilmartians/harmony/blob/678985febcbd45a94e396caef805b8065f59b6e4/package.json
- https://github.com/Splidejs/react-splide/blob/master/package.json
- https://github.com/vitejs/vite/discussions/2657
- https://jaketrent.com/post/package-json-style-attribute/
- https://kilianvalkhof.com/2022/css-html/your-css-reset-needs-text-size-adjust-probably/
- https://tailwindcss.com/docs/preflight
- https://tailwindcss.com/docs/theme#referencing-other-values
- https://tailwindcss.com/docs/preflight#accessibility-considerations
- https://tailwindcss.com/docs/installation
- https://github.com/tailwindlabs/tailwindcss/blob/v3.4.1/types/config.d.ts#L19: `theme(path: string, defaultValue?: unknown): any`
- https://developer.mozilla.org/en-US/docs/Learn/Getting_started_with_the_web/CSS_basics#anatomy_of_a_css_ruleset
- https://www.30secondsofcode.org/css/s/current-color/
- https://caniuse.com/extended-system-fonts
- https://unocss.dev/guide/style-reset
- https://github.com/unocss/unocss/tree/v0.58.5/packages/reset
- https://github.blog/changelog/2021-04-13-table-of-contents-support-in-markdown-files/
- `ButtonText`:
  - https://github.com/sindresorhus/modern-normalize/issues/24
  - https://www.w3.org/TR/css-color-3/#css-system
  - https://developer.mozilla.org/en-US/docs/Web/CSS/system-color

## Commands

```bash
npm install -D sort-package-json prettier npm-run-all2
```

```bash
npx tailwindcss --input test/preflight.css --output test/preflight.css
```

## Snippets

```markdown
`import "resetss/";`
```

```markdown
| File   | Source | Version |
| ------ | ------ | ------- |
| [``]() | []()   |         |
```
